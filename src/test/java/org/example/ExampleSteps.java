package org.example;

import static org.junit.Assert.assertEquals;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.lang.Exception;
/*
	Replace the class with your own step definition
   	classes.
 */
public class ExampleSteps {

    StringCalculator sc = new StringCalculator();
    int result;
    String exceptionMessage;

    @Given("an input to sum {string}")
    public void theInputToTheSum(String nums) {
        try {
            this.result = sc.add(nums);
        }
        catch (Exception e) {
            this.exceptionMessage = e.getMessage();
        }

    }

    @Then("the result is {int}")
    public void theResultOfTheSum(Integer int1) {
        assertEquals((int)int1, result);
    }

    @Then("an exception is raised with message {string}")
    public void theExceptionMessageIs(String givenMsg) {
        assertEquals(givenMsg, exceptionMessage);
    }
}