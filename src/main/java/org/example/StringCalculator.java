package org.example;

import java.util.ArrayList;

public class StringCalculator {

    public int add(String numbers) throws Exception {
        numbers = numbers.replace("\\n", "\n");
        String delimiters = ",";
        ArrayList<String> negatives = new ArrayList<String>();
        if (numbers.substring(0,2).equals("//")) {
            delimiters = numbers.split("\n")[0].substring(2);
            numbers = numbers.split("\n")[1];
        }
        int sum = 0;
        if (!numbers.isEmpty()) {
            for (String snum : numbers.split(delimiters)) {
                if(snum.charAt(0) == '-') {
                    negatives.add(snum);
                }
                else sum+= Integer.parseInt(snum);
            }
        }
        if (!negatives.isEmpty()) {
            String exceptionMsg = "negatives not allowed (found: ";
            for (String snum : negatives) {
                exceptionMsg += snum+", ";
            }
            exceptionMsg = exceptionMsg.substring(0,exceptionMsg.length()-2)+')';
            throw new Exception(exceptionMsg);
        }
        return sum;
    }
}
