Feature: Add operation

  Scenario: adding a basic string of numbers
    Given an input to sum "1,2,3"
    Then the result is 6

  Scenario: adding a string of numbers with modified separators
    Given an input to sum "///\n1/4/3"
    Then the result is 8

  #maybe we don't need to test for exceptions
  #Scenario: adding a string with negative numbers
  #  Given an input to sum "1,-3,2,-6,3"
  #  Then an exception is raised with message "negatives not allowed (found: -3, -6)"